//
//  main.m
//  ExistingProject
//
//  Created by Levi Santos on 29/07/14.
//  Copyright (c) 2014 Levi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LMSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LMSAppDelegate class]));
    }
}
